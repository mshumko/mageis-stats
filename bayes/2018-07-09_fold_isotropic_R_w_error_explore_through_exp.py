
# coding: utf-8

# # This notebook explores the uncertainty from folding a known exponential spectra through the MagEIS geometric factors with uncertainty

# In[1]:


import numpy as np
import pymc3 as pm
import matplotlib.pyplot as plt
#import seaborn as sns
import scipy.integrate
import scipy.interpolate
#sns.set(font_scale=1.5)
#get_ipython().run_line_magic('matplotlib', 'inline')
import sys
import h5py
import csv
import itertools

sys.path.append('../utils/')
import mageis_response


# Create a true exponential spectra with E0t and J0t parameters. Then use that spectra and fold it through the MagEIS response functions to get the true counts.

# In[2]:
saveFile = '2018-07-19_rerr_traces.h5'

# Spectra parameters
E0t = 100 # keV
J0t = 1E3 # 1/(cm^2 sr s keV)

# Initialize the response function
R = mageis_response.R_iso(101) # RBSP-A MagEIS LOW.
E = R.E*1000 # Energy array in keV.
FEDU_energy = [ 15.,  20.,  33.,  54.,  80., 108., 143., 184., 226.] # keV
n_ch = (1, 9) # Channels to use (1 through 9 are the main rate channels.)

# Interpolate the response functions so we can easily add uncertainty to them
# G is a function of energy that returns G(E) for all energy channels.
G = scipy.interpolate.interp1d(E, [R.response(i) for i in range(*n_ch)], axis=1)

# Define an exponential spectra.
def spectra(Earr, J0, E0):
    return(J0*np.exp(-Earr/E0))

# Now create the "true" counts drawn from the parent distribution. This assumes there
# is no uncertanity.
true_lambda = np.nan*np.zeros(n_ch[1]-n_ch[0])
for i in range(*n_ch):
    true_lambda[i-n_ch[0]] = scipy.integrate.trapz(spectra(E, J0t, E0t)*R.response(i), x=E)


# Since pymc3 does not natively do integrationm, now we code in a trapezium integrator (validated). Also define a pumc3 exponential spectra.
def pymcTrapz(f, x):
    """
    This function is the pymc3 Trapz implementation for an uneven grid.
    It integrates a function f over an uneven grid x.
    """
    return 0.5*pm.math.dot((f[1:] + f[:-1]), (x[1:] - x[:-1]))

def pymcExpSpectra(E, E0, J0):
    return J0*pm.math.exp(-E/E0)


plt.plot(E, spectra(E, J0t, E0t)); plt.title('True exponential energy spectra')
plt.yscale('log'); plt.xlabel('Energy (keV)'); plt.ylabel('Flux');


for i in range(*n_ch):
    plt.plot(E, R.response(i), label='ch{}'.format(i));
plt.xlabel('Energy (keV)'); plt.ylabel('G (cm^2 sr)'); 
plt.title('MagEIS response functions'); plt.yscale('log');
plt.legend(bbox_to_anchor=(1, 1)); #plt.savefig('response.png', dpi=300)


plt.semilogy(FEDU_energy[n_ch[0]:n_ch[1]], true_lambda, 'o'); 
plt.ylabel('counts'); plt.xlabel('Channel'); plt.title('True counts');


# Now add realism by drawing observed counts from true_lambda assuming a Poisson process

# In[7]:


np.random.seed(123)
c_obs = np.random.poisson(true_lambda)


# Visualize the counts derived from the spectra without uncertainty, and the counts that could have been observed if we assume a poisson process

# In[8]:


(_, caps, _) = plt.errorbar(FEDU_energy[n_ch[0]:n_ch[1]], c_obs, yerr=np.sqrt(c_obs), c='b', fmt='o', 
                            label='Observed counts', capsize=10)
for cap in caps:
    cap.set_markeredgewidth(1)
plt.plot(FEDU_energy[n_ch[0]:n_ch[1]], true_lambda, 'r*', ms=15, 
         label=r'$\lambda$ population')
plt.yscale('log'); plt.legend();


# ### Bayes model
# Here we explore how to back out the exponential spectra from the observed counts. We work with informed and uninformed spectral parameter priors (switch below) and add uncertnaity to the response functions (scale up/down, move left/right).
# 
# The number of divergences increases as the FWHM offset increases.

# In[ ]:


traces = {}
informedPrior = False # use uniform or normal priors on J0 and E0.

# This sets the R(E) scale to unity or normally distributed.
# If False (or 0), will not modify the response functions.
# Roffset = [10, 50, 100] # R(E) fwhm amplitude offset off if 0 (percent) 
# Eoffset = [0, 0, 0] # R(E) fwhm energy offset off if 0 (percent)

# Make a meshgrid of the amplitude and energy FWHm offsets.
N = 10
Roffset, Eoffset = np.meshgrid(np.arange(0, 101, N), np.arange(0, 101, N))

# Load the run log to see which files have already been run.
#with open('2018-07-09_run_log.csv', 'r') as f:
#    r = csv.reader(f)
#    next(r) # skip header
#    log = np.array(list(r), dtype=int)

# Open data file and check which runs have already been done
try:
    with h5py.File(saveFile, 'r') as hf:
            fKeys = list(hf.keys())
except OSError as err:
    if 'No such file or directory' in str(err):
        hf = h5py.File(saveFile, 'a')
        hf.close()
        fKeys = []
    else:
        raise

def getPriors(informedSpecP, Roffset, Eoffset):
    """
    This function handles the exponential spectra priors, 
    and can add uncertnaity to the G(E) emplitude and
    energy offset.
    """
    if informedSpecP:
        BoundedNormal = pm.Bound(pm.Normal, lower=0.0)
        J0 = BoundedNormal('J0', mu=1E6, sd=1E5)
        E0 = BoundedNormal('E0', mu=100, sd=500)
    else:
        J0 = pm.Uniform('J0', lower=0, upper=1E7)
        E0 = pm.Uniform('E0', lower=0, upper=500)
    
    # G(E) modifications
    if Roffset:
        Roff = pm.Normal('Roff', mu=1, 
                         sd=Roffset/(100*2*np.sqrt(2*np.log(2))), 
                         shape=len(c_obs))
    else:
        Roff = np.ones_like(c_obs)
    if Eoffset:
        Eoff = pm.Normal('Eoff', mu=1, 
                         sd=Eoffset/(100*2*np.sqrt(2*np.log(2))), 
                         shape=len(c_obs))
    else:
        Eoff = np.ones_like(c_obs)
    return E0, J0, Roff, Eoff

def lam(E0, J0, Roffset, Eoffset):
    """
    This function appears to the only way to correctly work with pymc3 and 
    propagate the sampled spectra though the instrument response functions.
    My implementation of trapezium is not the fastest, but it will do.
    """
    lams = len(c_obs)*[None]
    
    for i in range(len(c_obs)):
        J = pymcExpSpectra(E*Eoffset[i], E0, J0)
        #f is chanel-dependent offset * flux arr * interpolated 
        # G(E * a random energy offset)
        f = Roffset[i]*J*G(E)[i]  #G(E*Eoffset[i])[i] 
        lams[i] = pm.Deterministic('lam{}'.format(i), pymcTrapz(f, E))
    return lams

#for Rerr, Eerr in zip(Roffset, Eoffset):
for (i, j) in itertools.product(range(N+1), range(N+1)):
    Rerr = int(Roffset[i, j])
    Eerr = int(Eoffset[i, j])
    
    # Skip if that run has been done before. 
    if 'RERR{:02d}_EERR{:02d}'.format(Rerr, Eerr) in fKeys:
        continue
    
    m = pm.Model()
    print('Running with Rerr = {} % and Eerr = {} %'.format(Rerr, Eerr))
    with m:
        # Prior stocastic variables
        prior = getPriors(informedPrior, Rerr, Eerr)
        lams = lam(*prior) # Get prior determanistic variables

        c_m = pm.Poisson('c_m', mu=lams, observed=c_obs, shape=len(c_obs)) # Likeleyhood
        trace = pm.sample(1E4, tuning=1000, cores=3) # MCMC
    print('RERR{:02d}_EERR{:02d}'.format(Rerr, Eerr))
    traces['RERR{:02d}_EERR{:02d}'.format(Rerr, Eerr)] = trace

    # Save data
    with h5py.File(saveFile, 'a') as hf:
        #print('Existing file keys:', list(hf.keys()))
        for tKey in traces.keys():
            try:
                g = hf.create_group(tKey)
            except ValueError as err:
                if 'Unable to create group' in str(err):
                    print('tKey = ', tKey)
                    print('Existing file keys:', list(hf.keys()))
                    continue
                else:
                    raise

            g.create_dataset('E0', data=traces[tKey][:]['E0'])
            g.create_dataset('J0', data=traces[tKey][:]['J0'])
            fKeys.append('RERR{:02d}_EERR{:02d}'.format(Rerr, Eerr))
        try:
            g2 = hf.create_group('GLOBAL')
            g2.create_dataset('TRUE_E0', data=E0t)
            g2.create_dataset('TRUE_J0', data=J0t)
        except ValueError as err:
            #print('\n', str(err), '\n')
            if 'Unable to create group' in str(err):
                pass
            else:
                raise
