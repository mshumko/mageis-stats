# This function returns a Gaussian repsonse function for MagEIS energy 
# channels.

import numpy as np
import glob
import os
import scipy.integrate

import h5py

class R_gaus:
    def __init__(self):
        """
        This class creates gaussian response functions for the MagEIS LOW
        instrument.
        """
        
        return
        
    def _get_params(self, ch):
        """
        This method returns the channel middle and sigma.
        """
        mu = [35, 56, 80, 110.5, 145, 184, 226.5]
        sigma = [6, 10, 12, 15.5, 19, 20, 20.5]
        return mu[ch], sigma[ch]
    
    @property
    def mu(self):
        return [35, 56, 80, 110.5, 145, 184, 226.5]
    
    @property
    def sigma(self):
        return [6, 10, 12, 15.5, 19, 20, 20.5]
        
    def response(self, ch, R0, Earr):
        """
        This method returns a Gaussian reponse function for a particular
        energy channel (starting at 0), sampled at energies Earr. Amplitude
        is given by R0.
        """
        mu, sig = self._get_params(ch)
        R = R0*np.exp(-np.power(Earr - mu, 2.) / (2 * np.power(sig, 2.)))
        return R
    
class R_iso:
    def __init__(self, inst, lut=16385, rdir=None, magB=None):
        """
        This class handles the MagEIS response functions that are based on a
        geant4 model by Mark Looper, which was then combined into the 
        response functions by Paul O'Brien. This class calculates the 
        isotropic reponse functions for a given instrument.
        
        REQUIRED ARGS: inst = The MagEIS instrument. It is an integer e.g. 
                             101, 102 (RBSP-A/B low instrument). 
        OPTIONAL ARGS: lut = Look Up Table for instrument settings. This
                             parameter looks up the corresponding response
                             function file. Deafult is 16385
                       rdir = Top directory containing the response files.
                             default is './mageis_response_files/'.
                       magB = Magntude of the chamber magnetic field in 
                             10xYYY% e.g. 106.5% -> 1065. If none, this
                             code will look for the 100% B file.
        """
        # Set instrument parameters that will be used to find the response
        # function file.
        self.inst = inst
        if rdir is None:
            #self.rdir = '/home/msshumko/Documents/mageis-stats/utils/mageis_response_files/'
            #self.rdir = '/home/mike/research/mageis-stats/utils/mageis_response_files/'
            self.rdir = os.path.join(
                            os.path.abspath(os.path.dirname(__file__)), 
                            'mageis_response_files'
                                    )
        else:
            self.rdir = rdir
            
        self.magB = magB
        self.lut = lut
        self._load_file() # Load file
        return
            
        
    def _load_file(self):
        """
        This method uses the file parameters from the __init__ method to 
        find and open the response function file. 
        """
        if self.magB is None:
            s = os.path.join(self.rdir, '**/rbsp_mageis_*{}_MR_{}.mat'.format(
                self.inst, self.lut))
        else:
            s = os.path.join(self.rdir, '**/rbsp_mageis_*{}*{}*{}.mat'.format(
                self.inst, self.magB, self.lut))
                
        #s = os.path.join(self.rdir, 'high/rbsp_mageis_high_402_20130804_MRe_31747.mat')
        self.files = glob.glob(s)
        assert len(self.files)==1, ('Whoops, too many response function '
                                'files found! \n {}, {}'.format(
                                    s, self.files))
        self.f = h5py.File(self.files[0])
        return
    
    @property
    def _cal_file(self):
        return self.files[0]
        
    @property
    def E(self):
        return np.array(self.f['CHAN00']['ELE']['E_GRID'][0, :])
        
    def response(self, ch):
        """
        This method calculates the spin averaged response functions by 
        integrating out the angular components of the response functions
        for a specified channel ch.
        
        This function does the following integral (Paul's email):
        
        Here is a sample integral that converts from the 3-D R(E,th,ph) 
        tensor to a 1-D G(E) vector:
        G = trapz(-cosd(inst.TH_GRID),trapz(pi/180*inst.PH_GRID,inst.R,3),2); 
        % cm^2 sr

        cosd is cosine of an angle in degrees. By integrating vs -cosd(th) 
        it’s like integrating sin(th)dth. The phi integral is normal, 
        just with the degrees to radians conversion.
        """
        # Save variables for easy access.
        #self.E =  np.array(self.f['CHAN{:02d}'.format(ch)]['ELE']['E_GRID'][0, :])
        self.TH_GRID = np.array(self.f['CHAN{:02d}'.format(ch)]['ELE']['TH_GRID'][0, :])
        self.PH_GRID = np.array(self.f['CHAN{:02d}'.format(ch)]['ELE']['PH_GRID'][0, :])
        self.R = np.array(self.f['CHAN{:02d}'.format(ch)]['ELE']['R'][:])
        
        # Tile the PH_GRID and TH_GRID variables to do the integral.
        PH_GRID_TILE = np.swapaxes(
            np.swapaxes(
                np.tile(self.PH_GRID, (181, 300, 1)),
                 0, 2), 
             1, 2) # swapaxes is to get it into the same shape as self.R
        
        # Now assume an isotropic PA and gyrophase distribution and calculate 
        # the (G)eometric factor.
        #Gtemp = scipy.integrate.trapz(self.R, np.deg2rad(PH_GRID_TILE), axis=0)
        Gtemp = scipy.integrate.trapz(self.R, np.deg2rad(self.PH_GRID), axis=0)
        self.G = scipy.integrate.trapz(Gtemp,
                    x=-np.cos(np.deg2rad(self.TH_GRID)), axis=0
                    )
        return self.G
        
if __name__ == '__main__':
    import matplotlib.pyplot as plt

    iso = R_iso(101)
    
    for i in range(15):
        iso.response(i)
        plt.loglog(iso.E, iso.G, label='ch{}'.format(i))
    plt.legend()
    plt.show()
    
        
