import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import trapz
import mageis_response

class FluxSensitivity:
    def __init__(self, counts, sig=1, R=mageis_response.R_gaus,
                 Earr=np.linspace(0, 500, num=1000)):
        self.scounts = counts # Starting counts.
        
        # Determine the count bounds that will be considered valid for
        # the sensitivity analysis.
        self.uBound = self.scounts + sig*np.sqrt(self.scounts)
        self.lBound = self.scounts - sig*np.sqrt(self.scounts)
        self.R = R()
        self.R0 = 10
        self.Earr = Earr
        return
        
    def specParam(self, E0arr, J0arr):
        """
        This method creates a "dumb" grid of spectra parameters to try to
        fit to the data.
        """
        self.E0E0, self.J0J0 = np.meshgrid(E0arr, J0arr)
        return
        
    def modelCounts(self, E0arr, J0arr):
        """
        This method iterates over all possible combinations in the E0arr 
        and J0arr arrays and folds the spectra through the response 
        function to get the error on the counts. 
        
        Need to fold in the error in the flux as well. This should be 
        implemented next! It will require a monte-carlo, maybe a MCMC.
        """
        # Make meshgrid 
        self.specParam(E0arr, J0arr)
        # Cost function results go here.
        self.cost = np.nan*np.ones_like(self.E0E0) 
        # Save all of the count arrays
        self.cArr = np.nan*np.ones((*self.cost.shape, 7)) 
        
        # Loop over meshgrid
        (nE, nJ) = self.E0E0.shape
        for e in range(nE):
            for j in range(nJ):
                c = self.fold_spectra(self.E0E0[e, j], self.J0J0[e, j])
                self.cArr[e, j, :] = c
                # Simple test if folded counts are inside the desired
                # threshold counts.
                mask = ((self.uBound - c >= 0) & (c - self.lBound > 0))
                #print(mask)
                if np.all(mask):
                    self.cost[e, j] = 1
                else:
                    self.cost[e, j] = 0
        return
        
    def fold_spectra(self, E0, J0):
        """
        This is a simple method that folds a spectra with E0 and J0 
        parameters though a self.R response function.
        """
        counts = np.nan*np.ones(7)
        for ch in range(len(counts)):
            r = self.R.response(ch, self.R0, self.Earr)
            flux = J0*np.exp(-self.Earr/E0) # Exponential spectra.
            counts[ch] = trapz(r*flux, self.Earr)
        return counts
    
if __name__ == '__main__':
    c = [10000, 3000, 1000, 300, 100, 30, 10] # Fake count rates.
    f = FluxSensitivity(c, sig=15)
    E0arr = np.linspace(10, 15, 100); J0arr = np.linspace(300, 2000, 100)
    f.modelCounts(E0arr, J0arr)
    
    ### VISUALIZATION ###
    #plt.plot(c, 'ro')
    #plt.show()
    
    plt.pcolormesh(E0arr, J0arr, f.cost)
    plt.show()
