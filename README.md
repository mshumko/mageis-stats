This repo explores various Bayesian approaches to do uncertainty
quantifitcation for the MagEIS instrument.

The utils/ folder contains the mageis_response.py file which contains 
classes that model Gaussian instrument responses. This file also contains a 
class that returns the isotropic angular response functions from MagEIS. 
This class will need more modification, like to find correct files for 
particular Look Up Tables (LUTs). For these response function, the repo
need a folder called "mageis_response_files/" which contains > 1 GB of
response files for the different instruments, LUTs, and B field 
configurations.

The bayes/ folder contains the notebook files that explore various Bayesian
models. A few models worth mentioning:

- 2018-06-14_forward_model_single_count_exp_spectra.html is the first 
attempt with folding an arbitrary exponential energy spectra through a 
hypotheized (and idealized) instrument response function for multiple
 channels. This motebook shows full circle that we can go from a given 
spectra, fold it through the response functions, use MCMC to maximize the
likeleyhood of getting the observed counts given modeled counts. At the end
I visualize the uncertainty in the spectra parameters.

- 2018-06-15_forward_model_multi_count_exp_spectra.ipynb is very similar to
2018-06-14_forward_model_single_count_exp_spectra.html, but it explores how
the posterior of the exponential spectra parameters changes with multiple 
observations. The answer is that with more observations, the posterior 
distribution becomes tighter (as one would expect).

- 2018-06-27_fold_isotropic_R_through_exp.ipynb and
2018-06-28_fold_isotropic_R_through_powerlaw.ipynb explores the same models 
as before, mainly the exponential and power law spectrum, but the spectra 
is folded through the isotropic response functions from the RBSP-A's MagEIS
LOW detector.

- 2018-06-29_iso_R_arbitrary_spectra.ipynb contains the implementation of 
flux as a function of energy channel. Also, here I use the RBSP-A MagEIS
LOW detector's main rate channels (first 9 for LOW and MED, and first 7 for HIGH). This model works since it correctly produced the observed count rates. This model gives you flux and the uncertanity as a function of energy channel. At the end, I've attepted to fit the flux with an exponential and found values that are reasonable, but the J vs E curve does not look straight. I think this is due to the difficulty in describing the complex-looking response function with a single energy (channel middle).

The docs/ folder contains various documents provided by Paul O'Brien that 
describes the bowtie analysis which this Bayes approach closely follows 
(but is a very different approach). The PRBEM_Response_Format.doc doc 
contains the format to the .mat instrument response files. 
Response_Function_Library.docx contains the math necessary to convert from 
the instrument coordinate system to the magnetic cordinate system (pitch 
angle and gyrophase).

The plots/ folder contains the notebook plots, sorted by their generation 
date.
